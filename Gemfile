# frozen_string_literal: true
source 'https://rubygems.org'
ruby '2.5.0'

# Core
gem 'rails', '~> 5.0.6'
gem 'pg', '~> 0.18'
gem 'puma', '~> 3.0'
gem 'fast_blank'

# Authentication
gem 'devise', '~> 4.2.0' # authentication
gem 'omniauth'
gem 'omniauth-github'

# assets
gem 'sass-rails', '~> 5.0'
gem 'uglifier', '>= 1.3.0'
gem 'coffee-rails', '~> 4.2' # Coffeescript :(
gem 'jquery-rails' # jQuery
gem 'turbolinks', '~> 5' # Faster JS
gem 'hamlit' # faster haml
gem 'bootstrap' # Bootstrap, because laziness

# Redis!
gem 'redis'
gem 'hiredis' # faster
gem 'readthis' # caching
gem 'sidekiq' # jobs
gem 'sidekiq-failures' # failed jobs

# Other things
gem 'faraday' # HTTP connections
gem 'faraday_middleware' # More sophisticated HTTP connections

# ConvinceMe CI
gem 'octokit' # Make authenticate requests more easily
gem 'github-markup' # Render GitHub markup
gem 'commonmarker' # Required by github-markup
gem 'liquid-c' # GitHub pages rendering engine, processes things for our CI
gem 'tilt' # Templating handler

group :production do
  gem 'honeybadger' # error monitoring
  gem 'lograge' # Structure log data, put it in single lines to improve the functionality
  gem 'logstash-event' # Use logstash format for logging data
end

group :development do
  gem 'foreman'
  gem 'rerun'
end

group :development, :test do
  gem 'byebug', platform: :mri
end

group :test do
  gem 'rspec-rails'
  gem 'rails-controller-testing'
  gem 'guard'
  gem 'guard-rubocop'
  gem 'guard-rspec'
  gem 'factory_girl_rails'
  gem 'vcr'
end