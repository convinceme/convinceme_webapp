# frozen_string_literal: true
require 'sidekiq/web'
Sidekiq::Web.set :session_secret, Rails.application.secrets[:secret_key_base]
Rails.application.routes.draw do
  devise_for :users, controllers: { omniauth_callbacks: 'users/omniauth_callbacks' }

  post 'github_webhook_pull_request', to: 'github_hooks#pull_request'

  root 'landing#home'

  # authenticate :user, ->(u) { u.superuser? } do
    mount Sidekiq::Web => '/sidekiq'
  # end

  resources :votes, only: [:create]

  get 'reviews/:repository/:sha', to: 'reviews#show'

  get 'content_instructions', to: redirect('https://github.com/convinceme/info/blob/master/content_guidelines.markdown')

  get 'not_found', to: 'errors#not_found'
  match '/404', to: 'errors#not_found', via: :all

  resources :debates, only: [:show, :edit, :update]
  get '*id', to: redirect('/debates/%{id}')
end
