desc 'update all the arguments'
task update_all_arguments: :environment do
  UpdateAllArgumentsJob.perform_later
end

desc 'update all the categories and topics'
task update_categories_and_topics: :environment do
  UpdateCategoriesAndTopicsJob.perform_later
end

task make_commit_review: :environment do
  ReviewCommitJob.perform_later(repository: 'arguments_against',
                                sha: '45c4b75bc14a64f23717be39657ed9f2e7b5655b',
                                pr_number: 23,
                                pr_action: 'open')
end