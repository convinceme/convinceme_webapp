# frozen_string_literal: true
class Category < ApplicationRecord
  include Sluggable
  has_many :change_categories, dependent: :destroy
  has_many :associated_changes, through: :change_categories
end
