# frozen_string_literal: true
class User < ApplicationRecord
  devise :database_authenticatable, :registerable, :recoverable, :rememberable,
         :trackable, :validatable, :omniauthable, omniauth_providers: [:github]
  has_many :votes

  def self.from_omniauth(auth)
    where(provider: auth.provider, uid: auth.uid).first_or_create do |user|
      user.email = "githuboauthed+#{auth.uid}@convinceme.us" # Devise needs something, and I don't want to disable email because future
      user.github_username = auth.info.nickname
      user.password = Devise.friendly_token[0, 20]
    end
  end

  def display_name
    github_username
  end

  def can_vote?(kind: nil, topic: nil)
    return false unless topic && kind && Vote::KINDS.include?(kind)
    matching_vote = votes.where(topic_id: topic.id, kind: kind)
                         .order(created_at: :desc).limit(1).first
    !(matching_vote && matching_vote.created_at > topic.updated_at)
  end

  def calculate_score
    votes.count # Will include contribution count soon
  end
end
