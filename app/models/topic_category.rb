# frozen_string_literal: true
class TopicCategory < ApplicationRecord
  belongs_to :topic
  belongs_to :category
  validates_uniqueness_of :topic_id, scope: [:category_id]
end
