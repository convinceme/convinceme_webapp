# frozen_string_literal: true
module Sluggable
  extend ActiveSupport::Concern
  included do
    validates :name, presence: true
    before_validation :set_slug
    validates :slug, uniqueness: true
  end

  module ClassMethods
    def slugify(str)
      str && str.to_s
                .gsub(/\([^\)]*\)/, '').strip
                .gsub(/(\s|\-|\+)+/, '_').downcase
                .gsub(/([^A-Za-z0-9_]+)/, '')
    end

    def friendly_find_id(n)
      o = friendly_find(n)
      o.present? ? o.id : nil
    end

    def friendly_find_slug(n)
      find_by_slug(slugify(n))
    end

    def friendly_find(n)
      return nil unless n.present?
      integer_slug?(n) ? find(n) : friendly_find_slug(n)
    end

    def integer_slug?(n)
      n.is_a?(Integer) || n.match(/\A\d*\z/).present?
    end
  end

  def to_param
    slug
  end

  def set_slug
    self.slug = self.class.slugify(name)
  end
end
