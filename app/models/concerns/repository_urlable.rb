# frozen_string_literal: true
module RepositoryUrlable
  extend ActiveSupport::Concern

  def repository_url
    "https://github.com/convinceme/#{repository}"
  end
end
