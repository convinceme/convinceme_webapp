# frozen_string_literal: true
class CommitReview < ApplicationRecord
  include RepositoryUrlable
  STATUSES = %w(pending success error failure).freeze
  validates_presence_of :sha
  validates_presence_of :repository
  validates :status, inclusion: {
    in: STATUSES,
    message: "%{value} is not one of: #{STATUSES.to_sentence}"
  }

  def self.friendly_find_by_sha(str)
    if str.length > 7
      find_by_sha(str)
    else
      where('sha LIKE ?', "#{str}%").first
    end
  end

  def self.output_file_review_template
    Rails.root.join('app', 'views', 'reviews', '_file_review_template.erb')
  end

  def perspective
    Perspective.find_by_repository(repository)
  end

  def create_output_file
    output_filepath.dirname.mkdir unless output_filepath.dirname.directory?
    FileUtils.touch output_filepath
  end

  def output_filepath
    if Rails.env.production?
      Pathname.new("#{ENV['STACK_BASE']}/shared/commit_reviews/#{repository}/#{sha}.html")
    elsif Rails.env.development?
      Rails.root.join('public', 'commit_reviews', repository, "#{sha}.html")
    else
      Rails.root.join('tmp', 'commit_reviews', "#{sha}.html")
    end
  end

  def short_sha
    # When we run into a collision, we'll fix it
    sha[0...7]
  end

  def file_content(filename)
    result = GithubApi.new.raw_file_fetch(raw_file_url(filename)).get
    result.status == 200 && result.body
  end

  def pr_url
    "#{repository_url}/pulls/#{pr_number}"
  end

  def raw_file_url(filename)
    file_hash_for(filename)['raw_url']
  end

  def file_hash_for(filename)
    changed_files['files'].detect { |cf| cf['filename'] == filename }
  end

  def filename_reviewed?(filename)
    File.foreach(output_filepath).any? { |l| l["data-filename=\"#{filename}\""] }
  end

  def filenames
    changed_files && changed_files['files'].map { |h| h['filename'] }
  end

  def argument_filenames
    filenames.select { |f| f[/\.ma?r?k?do?w?n?\z/i].present? }
             .reject { |f| f[/readme.m/i].present? }
  end

  def all_files_reviewed?
    !argument_filenames.detect { |f| !filename_reviewed?(f) }
  end

  def status_description
    return status_details if status_details.present?
    return 'Building the preview right now' if status == 'pending'
    return 'Changes permitted' if status == 'success'
  end

  def github_status_hash
    {
      target_url: "#{ENV['BASE_URL']}/reviews/#{repository}/#{sha}",
      description: status_description,
      context: 'ConvinceMe Argument Review'
    }
  end
end
