# frozen_string_literal: true
class Topic < ApplicationRecord
  include Sluggable
  has_many :arguments, dependent: :destroy
  has_many :votes, through: :arguments
  has_many :topic_categories, dependent: :destroy
  has_many :categories, through: :topic_categories
  belongs_to :parent_topic, class_name: 'Topic', required: false
  has_many :child_topics, foreign_key: 'parent_topic_id', class_name: 'Topic'

  scope :top_level, -> { where(parent_topic_id: nil) }
  scope :not_top_level, -> { where.not(parent_topic_id: nil) }
  scope :alpha, -> { order(:name) }

  def self.friendly_find_slug(n) # Overrides sluggable method
    s = slugify(n)
    find_by_slug(s) || find_by_previous_slug(s)
  end

  before_save :set_previous_slug
  def set_previous_slug
    self.previous_slug ||= slug
  end

  def self.friendly_find_id_from_filename(filename)
    friendly_find_id(filename.gsub(/\.ma?r?k?do?w?n?\z/i, ''))
  end

  def category_names
    categories && categories.pluck(:name) || []
  end

  def category_names=(vals)
    valid_cat_ids = vals.map { |v| Category.friendly_find(v).id }
    existing_cat_ids = topic_categories.pluck(:category_id)
    (existing_cat_ids - valid_cat_ids).each { |id| topic_categories.find_by_category_id(id).destroy }
    valid_cat_ids.each { |id| topic_categories.build(category_id: id) unless existing_cat_ids.include?(id) }
    true
  end

  def perspective_hashes
    Rails.cache.fetch("#{cache_key}_perspective_hashes") do
      enum_letters = %w(A B C D E F G H I J K)
      Perspective.pluck(:id).shuffle.map do |p_id|
        arg = arguments.where(perspective_id: p_id).first
        {
          perspective_id: p_id,
          argument_id: arg && arg.id,
          enum_letter: enum_letters.shift
        }
      end
    end
  end
end
