# frozen_string_literal: true
class Perspective < ApplicationRecord
  include Sluggable
  include RepositoryUrlable
  has_many :arguments
  validates_uniqueness_of :repository
  before_validation :set_default_repository

  def self.pro
    where(name: 'Arguments For'.freeze).first_or_create
  end

  def self.con
    where(name: 'Arguments Against'.freeze).first_or_create
  end

  def pro?; name == 'Arguments For'.freeze end
  def con?; name == 'Arguments Against'.freeze end

  def api_url
    "https://api.github.com/repos/convinceme/#{repository}"
  end

  def set_default_repository
    self.repository ||= slug
  end

  # Override sluggable to make this 
  def set_slug
    self.slug = case
    when pro? then 'pro'
    when con? then 'con'
    else
      self.slug = self.class.slugify(name)
    end
  end
end
