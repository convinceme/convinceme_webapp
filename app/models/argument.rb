# frozen_string_literal: true

class Argument < ApplicationRecord
  belongs_to :topic
  belongs_to :perspective
  validates_uniqueness_of :topic_id, scope: [:perspective_id]
  has_many :votes

  def self.character_limit
    1400
  end

  def self.default_liquid_layout
    File.read(Rails.root.join('app', 'views', 'arguments', 'liquid_template.html'))
  end

  def self.reference_keys
    %i[title links author publisher date_published]
  end

  def github_pages_url
    "https://convinceme.github.io/#{perspective.repository}/#{topic_url}"
  end

  def topic_url
    URI.escape(topic.name)
  end

  # I planned this interaction out with a new "body_raw" attribute, but now I'm not sure
  # I'm going to wait to implement this until I actually add the processing bits
  def fetch_body_html
    # In here we:
    #   return '' if body_raw is blank
    #   return body_html if it's present
    #   render the body and update body_html if there isn't a stored version of the body
  end

  def body
    # fetch_body_html & process to grab the body without the references
  end

  def references
    # fetch_body_html & process to grab the references
    []
  end

  def empty_reference
    self.class.reference_keys.map { |k| [k, nil] }.to_h
  end
end
