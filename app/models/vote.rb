# frozen_string_literal: true
class Vote < ApplicationRecord
  belongs_to :argument
  belongs_to :user
  belongs_to :topic
  has_one :perspective, through: :argument
  KINDS = %w(opinion quality).freeze
  validates :kind, inclusion: {
    in: KINDS,
    message: "%{value} is not one of: #{KINDS.join(' or ')}"
  }

  before_validation :set_topic
  def set_topic
    self.topic_id = argument && argument.topic_id
  end
end
