# frozen_string_literal: true
## This is the object we use for interacting with GitHub's API
## this contains anything we use multiple times
## Using v3, look at the docs here: https://developer.github.com/v3
require 'faraday_middleware'
class GithubApi
  API_BASE_URL = 'https://api.github.com/'

  class << self
    def parsed_timestamp(time)
      Time.parse(time)
    end

    def accept_header
      'application/vnd.github.v3+json'
    end
  end

  def connection(path, url: nil)
    Faraday.new(url: url || "#{API_BASE_URL}#{path}") do |f|
      f.request :url_encoded
      f.headers['Accept'] = self.class.accept_header
      f.adapter Faraday.default_adapter
    end
  end

  def pull_request_connection(repository:, pr_number:)
    connection "repos/convinceme/#{repository}/pulls/#{pr_number}"
  end

  def raw_file_fetch(url)
    Faraday.new(url) do |f|
      f.use FaradayMiddleware::FollowRedirects
      f.adapter :net_http
    end
  end

  def octokit_client
    Octokit::Client.new(access_token: ENV['CONVINCEME_BOT_ACCESS_TOKEN'])
  end

  def update_pr_status(commit_review)
    octokit_client.create_status("convinceme/#{commit_review.repository}",
                                 commit_review.sha,
                                 commit_review.status, 
                                 commit_review.github_status_hash)
  end
end
