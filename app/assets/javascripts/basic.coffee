window.convinceApp or (window.convinceApp = {})

convinceApp.init = ->
  if $('.editing-debate').length
    convinceApp.initDebateEdit()

convinceApp.initDebateEdit = ->
  window.emptyReference = $('.individual-reference.emptyRef')[0].outerHTML
  # Show the add reference link (hidden by default, because if no js)
  $('.add-additional-reference').collapse('show')
  # Add a new reference fields when the reference button is clicked
  $('.add-additional-reference').click (e) =>
    e.preventDefault()
    id = $('.individual-reference').length # Because index starts at 0, we aren't off by 1
    $('.references-form-fields').prepend(window.emptyReference.replace(/reference_0_/g, "reference_#{id}_"))

$(document).on 'turbolinks:load', ->
  convinceApp.init()
