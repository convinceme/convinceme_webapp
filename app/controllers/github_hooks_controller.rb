# frozen_string_literal: true
class GithubHooksController < ApplicationController
  skip_before_action :verify_authenticity_token
  before_action :verify_signature

  def pull_request
    if params['pull_request']
      ReviewCommitJob.perform_later(repository_arguments)
      MergedPullRequestJob.set(wait: 30.seconds).perform_later(merged_pr_args)
      # Because it sometimes takes time for github pages to render
      MergedPullRequestJob.set(wait: 5.minutes).perform_later(merged_pr_args)
    end
  end

  private

  def merged_pr_args
    repository_arguments.slice(:repository, :pr_number)
  end

  def repository_arguments
    {
      repository: permitted_params['repository']['name'],
      pr_number: permitted_params['number'],
      pr_action: permitted_params['action'],
      sha: permitted_params['pull_request']['head']['sha']
    }
  end

  def verify_signature
    signature = 'sha1=' + OpenSSL::HMAC.hexdigest(OpenSSL::Digest.new('sha1'), ENV['GITHUB_HOOK_SECRET'], request.body.read)
    raise "Signatures didn't match!" unless Rack::Utils.secure_compare(signature, request.env['HTTP_X_HUB_SIGNATURE'])
  end

  def permitted_params
    JSON.parse(request.body.read) # Because params[:action] is overwritten
  end
end
