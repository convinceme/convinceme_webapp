# frozen_string_literal: true
class LandingController < ApplicationController
  def home; end
end
