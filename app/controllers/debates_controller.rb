# frozen_string_literal: true

class DebatesController < ApplicationController
  before_action :find_topic

  def show
    @title = @topic.name
    @display_char_count = ActiveRecord::Type::Boolean.new.cast(params[:count])
  end

  def edit
    @argument ||= @topic.arguments.where(perspective_id: perspective.id).first_or_initialize
  end

  def update
  end

  helper_method :perspective

  private

  def find_topic
    @topic = Topic.friendly_find(params[:id])
    return @topic if @topic.present?
    render(not_found_path, status: 404)
    return
  end

  def perspective
    @perspective ||= params[:perspective] == 'con' ? Perspective.con : Perspective.pro
  end
end
