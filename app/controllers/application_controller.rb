# frozen_string_literal: true
class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  force_ssl if: :ssl_configured?

  def ssl_configured?
    Rails.env.production?
  end

  def vote_for_argument(vote_params)
    argument = Argument.where(id: vote_params[:argument_id]).first
    topic = Topic.where(id: argument && argument.topic_id).first
    if current_user.can_vote?(kind: vote_params[:kind], topic: topic)
      vote = Vote.new(vote_params.merge(user_id: current_user.id))
      if vote.save
        current_user.update_attribute :score, current_user.score + 1
      else
        flash[:error] = vote.errors.full_messages.to_sentence
      end
    else
      flash[:error] = unable_to_vote_message(topic)
    end
    topic && "/#{topic.slug}" || request.referrer
  end

  def after_sign_in_path_for(resource)
    if session[:after_login_vote].present?
      vote_for_argument(session.delete(:after_login_vote))
    else
      request.env['omniauth.origin'] || stored_location_for(resource) || root_path
    end
  end

  private

  def unable_to_vote_message(topic)
    if topic
      "You aren't allowed to vote on #{topic.name} right now"
    else
      "Sorry, don't know what you were trying to vote on"
    end
  end
end
