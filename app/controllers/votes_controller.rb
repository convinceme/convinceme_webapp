# frozen_string_literal: true
class VotesController < ApplicationController
  def create
    return store_vote_and_redirect unless current_user.present?
    redirect_to vote_for_argument(permitted_params)
  end

  private

  def store_vote_and_redirect
    session[:after_login_vote] = {
      argument_id: params[:vote][:argument_id],
      kind: params[:vote][:kind]
    }
    redirect_to new_user_session_path
  end

  def permitted_params
    params.require(:vote).permit(:argument_id, :kind)
  end
end
