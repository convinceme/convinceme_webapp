# frozen_string_literal: true
class ReviewsController < ApplicationController
  def show
    @commit_review = CommitReview.where(repository: params[:repository])
                                 .friendly_find_by_sha(params[:sha])
    unless @commit_review.present?
      render(not_found_path, status: 404)
      return
    end
  end
end
