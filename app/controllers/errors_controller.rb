# frozen_string_literal: true
class ErrorsController < ApplicationController
  respond_to :html, :json
  before_filter :set_permitted_format

  def not_found
    render(status: 404)
  end

  private

  def set_permitted_format
    request.format = 'html' unless request.format == 'json'
  end
end
