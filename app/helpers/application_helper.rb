# frozen_string_literal: true
module ApplicationHelper
  def bootstrap_class_for(flash_type)
    {
      success: 'alert-success',
      error: 'alert-danger',
      alert: 'alert-warning',
      notice: 'alert-info'
    }.fetch(flash_type.to_sym)
  end

  def argument_links(perspective_hashes, kind:)
    perspective_hashes.map do |perspective_hash|
      button_to perspective_hash[:enum_letter],
                '/votes',
                arg_btn_params(perspective_hash[:argument_id], kind)
    end.to_sentence(two_words_connector: ' or ', last_word_connector: ' or ')
       .html_safe + '?'
  end

  private

  def arg_btn_params(id, kind)
    {
      method: 'post',
      params: {
        vote: {
          argument_id: id,
          kind: kind
        }
      }
    }
  end
end
