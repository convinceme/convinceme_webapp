# frozen_string_literal: true
class ReviewCommitJob < ApplicationJob
  queue_as :default
  def perform(repository:, sha:, pr_number:, pr_action:)
    return 'Not reviewed action' unless %(opened synchronize).include?(pr_action)
    return 'Not reviewed repository' unless Perspective.pluck(:repository).include?(repository)
    commit_review = find_or_create_commit_review(repository, sha, pr_number)
    return 'Unable to create commit_review' unless commit_review.present?
    changed_filenames(commit_review)
      .each { |f| ReviewCommitFileJob.perform_later(commit_review: commit_review, filename: f) }
    GithubApi.new.update_pr_status(commit_review)
  end

  def find_or_create_commit_review(repository, sha, pr_number)
    attrs = { repository: repository, sha: sha, pr_number: pr_number }
    CommitReview.where(attrs).first || CommitReview.new(attrs.merge(status: 'pending'))
  end

  def changed_filenames(commit_review)
    commit_review.filenames ? commit_review.argument_filenames : store_changed_files(commit_review)
  end

  def store_changed_files(commit_review)
    commit_review.changed_files = {
      'files' => fetch_changed_files(commit_review) || []
    }
    commit_review.save && commit_review.create_output_file
    commit_review.argument_filenames || []
  end

  def fetch_changed_files(commit_review)
    result = GithubApi.new.connection("/repos/convinceme/#{commit_review.repository}/pulls/#{commit_review.pr_number}")
                      .get('files')
    result && JSON.parse(result.body)
  end
end
