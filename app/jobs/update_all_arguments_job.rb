# frozen_string_literal: true
class UpdateAllArgumentsJob < ApplicationJob
  queue_as :default

  def perform(topic = nil)
    topic_ids = topic && [topic.id] || Topic.pluck(:id)
    perspective_ids = Perspective.pluck(:id)
    topic_ids.each do |topic_id|
      perspective_ids.each do |perspective_id|
        UpdateArgumentJob.perform_later(Argument.where(topic_id: topic_id, perspective_id: perspective_id).first)
      end
    end
  end
end
