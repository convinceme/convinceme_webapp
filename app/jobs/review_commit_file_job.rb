# frozen_string_literal: true
require 'erb'
require 'liquid'
require 'tilt'
require 'github/markup'
# Thanks to https://github.com/jekyll/jekyll/blob/56dfe18c5dd2ac769670480600e5e1c63ecdac8d/lib/jekyll/document.rb
# for the parsing of the yaml front matter
class ReviewCommitFileJob < ApplicationJob
  queue_as :default
  YAML_FRONT_MATTER_REGEXP = %r!\A(---\s*\n.*?\n?)^((---|\.\.\.)\s*$\n?)!m

  def perform(commit_review:, filename:)
    return true if commit_review.filename_reviewed?(filename)
    file_content = commit_review.file_content(filename)
    return nil unless file_content
    str = rendered_template(commit_review, filename, file_content)
    return true if commit_review.filename_reviewed?(filename)
    File.open(commit_review.output_filepath, 'a') { |f| f.puts str }
    UpdateStatusAndTellGithubJob.perform_later(commit_review)
  end

  def rendered_template(commit_review, filename, file_content)
    liquid = rendered_file_content_template(file_content, default_data_for(commit_review, filename))
    template = Tilt.new(CommitReview.output_file_review_template)
    template.render(self, 'filename' => filename, 'liquid' => liquid, 'too_long' => too_long?(liquid))
  end

  def rendered_file_content_template(file_content, data)
    if file_content =~ YAML_FRONT_MATTER_REGEXP
      file_content = $POSTMATCH.force_encoding('utf-8')
      data['page'] = YAML.safe_load(Regexp.last_match(1), [Date])
    end
    data['content'] = GitHub::Markup.render_s(GitHub::Markups::MARKUP_MARKDOWN, file_content)
    Liquid::Template.parse(Argument.default_liquid_layout)
                    .render(data)
  end

  def default_data_for(commit_review, filename)
    {
      'repository_url' => commit_review.repository_url,
      'uri_escaped_page_name' => URI.encode(filename)
    }
  end

  def too_long?(liquid)
    liquid.split('character-count').last.gsub(/\D/, '')
          .to_i > Argument.character_limit
  end
end
