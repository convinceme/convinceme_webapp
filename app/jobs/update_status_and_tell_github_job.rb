# frozen_string_literal: true
class UpdateStatusAndTellGithubJob < ApplicationJob
  queue_as :default
  def perform(commit_review)
    return nil unless commit_review.all_files_reviewed?
    commit_review.status_details = check_content_issues(file_reviews_content(commit_review), commit_review.argument_filenames.count)
    commit_review.status = commit_review.status_details.present? ? 'failure' : 'success'
    commit_review.save
    GithubApi.new.update_pr_status(commit_review)
  end

  def file_reviews_content(commit_review)
    File.read(commit_review.output_filepath)
  end

  def check_content_issues(content, argument_file_count)
    errors = [
      image_error(content),
      # illegal_link(content),
      # illegal_section(content, argument_file_count), # should implement this
      # illegal_article(content, argument_file_count), # also this
      over_character_count(content)
    ].compact
    errors.any? ? errors.to_sentence : nil
  end

  def image_error(content)
    return nil unless content[/<img/i].present?
    'image tags are not permitted'
  end

  def over_character_count(content)
    return nil unless content[/character-count-over\">/].present?
    'too long of an argument'
  end

  def illegal_section(content, argument_file_count)
    # There are two sections per argument rendered with liquid
    return nil unless content.split(/<section/i).count * 2 > argument_file_count
    'you are not allowed to include section tags'
  end

  def illegal_article(content, argument_file_count)
    return nil unless content.split(/<article/i).count > argument_file_count
    'you are not allowed to include article tags'
  end

  def illegal_link(content)
    # TODO: implement this ;)
    non_reference = content.gsub(/\s+/, ' ').split(/<section/i)
                           .reject { |c| c[/\A\s?class=.references/i].present? }
                           .join(' ')
    return nil unless non_reference[/<a href=.http/i].present?
    'you are not allowed to include links not in the reference section'
  end
end
