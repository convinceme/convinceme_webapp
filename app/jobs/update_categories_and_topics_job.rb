# frozen_string_literal: true
class UpdateCategoriesAndTopicsJob < ApplicationJob
  queue_as :default
  TOPICS_TSV_URL = 'https://raw.githubusercontent.com/convinceme/info/master/topics.tsv'
  CATEGORIES_TSV_URL = 'https://raw.githubusercontent.com/convinceme/info/master/categories.tsv'
  TSV_OPTS = { headers: true, header_converters: :symbol, col_sep: "\t" }.freeze
  require 'csv'

  def perform
    processed_categories.each do |category_attrs|
      next unless category_attrs[:name].present?
      category = Category.friendly_find(category_attrs[:name]) || Category.new
      next if category.name == category_attrs[:name]
      category.update_attributes category_attrs
    end
    processed_topics.each do |topic_attrs|
      next unless topic_attrs[:name].present?
      topic = Topic.friendly_find(topic_attrs[:name]) || Topic.new
      next if topic.name == topic_attrs[:name] &&
              topic.category_names == topic_attrs[:category_names] &&
              topic.parent_topic_id == topic_attrs[:parent_topic_id]
      topic.update_attributes topic_attrs
    end
    create_missing_arguments
  end

  def create_missing_arguments
    perspective_ids = Perspective.pluck(:id)
    Topic.pluck(:id).each do |topic_id|
      perspective_ids.each do |perspective_id|
        Argument.where(topic_id: topic_id, perspective_id: perspective_id)
                .first_or_create
      end
    end
  end

  def processed_categories
    CSV.parse(Faraday.get(CATEGORIES_TSV_URL).body, TSV_OPTS)
       .map { |row| process_category(row.to_hash) }
  end

  def processed_topics
    CSV.parse(Faraday.get(TOPICS_TSV_URL).body, TSV_OPTS)
       .map { |row| process_topic(row.to_hash) }
  end

  def process_category(hash)
    {
      name: hash[:category]
    }
  end

  def process_topic(hash)
    {
      name: hash[:topic],
      category_names: hash[:categories].split(',').reject(&:blank?).map(&:strip),
      parent_topic_id: Topic.friendly_find_id(hash[:parent_topic])
    }
  end
end
