# frozen_string_literal: true
class MergedPullRequestJob < ApplicationJob
  queue_as :default

  def perform(repository:, pr_number:)
    api_connection = GithubApi.new.pull_request_connection(repository: repository, pr_number: pr_number)
    return nil unless merged?(api_connection.get('merge'))
    # We only care about info, content_layout and perspectives pull requests rn
    return UpdateCategoriesAndTopicsJob.perform_later if repository == 'info'
    perspective = Perspective.find_by_repository(repository)
    return nil unless perspective
    modified_topic_ids(api_connection.get('files')).each do |topic_id|
      UpdateArgumentJob.perform_later(perspective.arguments.find_by_topic_id(topic_id))
    end
  end

  def merged?(merge_response)
    merge_response.status == 204
  end

  def modified_topic_ids(files_response)
    pull_request_files(files_response).map do |filename|
      Topic.friendly_find_id_from_filename(filename)
    end.compact
  end

  def pull_request_files(files_response)
    JSON.parse(files_response.body).map { |f| f['filename'] }
  end
end
