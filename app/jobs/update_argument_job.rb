# frozen_string_literal: true
class UpdateArgumentJob < ApplicationJob
  queue_as :default

  def perform(argument)
    new_html = github_pages_html(argument.github_pages_url)
    return true unless new_html != argument.body_html
    argument.update_attributes(body_html: new_html)
    argument.topic.touch # Bust that topic cache
  end

  def github_pages_html(url)
    result = Faraday.get(url)
    return nil unless result.status == 200
    result.body
  end
end
