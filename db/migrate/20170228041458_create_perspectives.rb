class CreatePerspectives < ActiveRecord::Migration[5.0]
  def change
    create_table :perspectives do |t|
      t.string :name
      t.string :slug
      t.string :repository

      t.timestamps
    end
  end
end
