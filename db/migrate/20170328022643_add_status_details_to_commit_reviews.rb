class AddStatusDetailsToCommitReviews < ActiveRecord::Migration[5.0]
  def change
    add_column :commit_reviews, :status_details, :text
  end
end
