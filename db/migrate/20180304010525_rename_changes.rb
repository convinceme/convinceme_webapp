class RenameChanges < ActiveRecord::Migration[5.0]
  def change
    rename_table :changes, :topics
    rename_table :change_categories, :topic_categories
    rename_column :topics, :parent_change_id, :parent_topic_id
    rename_column :topic_categories, :change_id, :topic_id
    rename_column :arguments, :change_id, :topic_id
    rename_column :votes, :change_id, :topic_id
  end
end
