class RenameMeasuresToChanges < ActiveRecord::Migration[5.0]
  def change
    remove_reference :arguments, :measure
    remove_reference :votes, :measure
    remove_reference :measure_categories, :measure
    rename_table :measures, :changes
    rename_table :measure_categories, :change_categories
    add_reference :arguments, :change
    add_reference :change_categories, :change
    add_reference :votes, :change
  end
end
