class CreateCommitReviews < ActiveRecord::Migration[5.0]
  def change
    create_table :commit_reviews do |t|
      t.text :sha
      t.string :repository
      t.string :status
      t.integer :pr_number
      t.text :pr_name
      t.json :changed_files

      t.timestamps
    end
  end
end
