class CreateVotes < ActiveRecord::Migration[5.0]
  def change
    create_table :votes do |t|
      t.references :user
      t.references :argument
      t.references :measure
      t.string :kind

      t.timestamps
    end
  end
end
