class AddParentChangeToChanges < ActiveRecord::Migration[5.0]
  def change
    add_reference :changes, :parent_change
  end
end
