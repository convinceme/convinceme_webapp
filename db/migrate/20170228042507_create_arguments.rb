class CreateArguments < ActiveRecord::Migration[5.0]
  def change
    create_table :arguments do |t|
      t.references :topic
      t.references :perspective
      t.text :body_html

      t.timestamps
    end
  end
end
