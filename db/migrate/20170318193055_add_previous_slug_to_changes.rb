class AddPreviousSlugToChanges < ActiveRecord::Migration[5.0]
  def change
    add_column :changes, :previous_slug, :string
  end
end
