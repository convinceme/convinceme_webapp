class RenameTopicToMeasure < ActiveRecord::Migration[5.0]
  def change
    remove_reference :arguments, :topic
    remove_reference :topic_categories, :topic
    rename_table :topics, :measures
    rename_table :topic_categories, :measure_categories
    add_reference :arguments, :measure
    add_reference :measure_categories, :measure
  end
end
