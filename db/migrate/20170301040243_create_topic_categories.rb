class CreateTopicCategories < ActiveRecord::Migration[5.0]
  def change
    create_table :topic_categories do |t|
      t.references :topic
      t.references :category

      t.timestamps
    end
  end
end
