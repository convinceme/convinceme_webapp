# frozen_string_literal: true
[
  { name: 'Arguments For', repository: 'arguments_for' },
  { name: 'Arguments Against', repository: 'arguments_against' }
].each { |i| Perspective.create(i) }

UpdateCategoriesAndTopicsJob.new.perform
UpdateAllArgumentsJob.new.perform
