# frozen_string_literal: true
require 'rails_helper'

RSpec.describe ReviewCommitFileJob, type: :job do
  let(:subject) { ReviewCommitFileJob.new }
  let(:commit_review) { FactoryGirl.create(:commit_review) }

  describe 'perform' do
    context 'already reviewed' do
      let(:filename) { 'something_sweet.markdown' }
      xit 'returns true if the file has already been reviewed' do
        expect(commit_review).to receive(:filename_reviewed?).with(filename) { true }
        expect(subject.perform(commit_review: commit_review, filename: filename)).to be_truthy
      end
    end
    context 'not reviewed' do
      before do
        commit_review.create_output_file
        File.truncate(commit_review.output_filepath, 0)
        ActiveJob::Base.queue_adapter = :test
      end
      let(:output_file_review_template) { Rails.root.join('spec', 'fixtures', 'commit_review_stubbed_output_file_review_template.erb') }
      xit 'appends the liquid template to the commit_review output_file' do
        expect(CommitReview).to receive(:output_file_review_template) { output_file_review_template }
        expect(Argument).to receive(:default_liquid_layout) { '{% increment ref_number %} {{ content }}' }
        expect(commit_review).to receive(:file_content) { 'lololol' }
        expect do
          subject.perform(commit_review: commit_review, filename: 'filename')
        end.to have_enqueued_job(UpdateStatusAndTellGithubJob).with(commit_review)
        output = File.read(commit_review.output_filepath)
        expect(output).to eq "<h1>stuff</h1>\n0 <p>lololol</p>\n"
      end
    end
  end
  describe 'rendered things' do
    let(:target_output) { File.read(target_filepath).gsub(/\s+/, ' ').strip }
    describe 'render_file_content_template' do
      let(:markdown_filepath) { Rails.root.join('spec', 'fixtures', 'review_commit_file_job-test-markdown-content.markdown') }
      let(:target_filepath) { Rails.root.join('spec', 'fixtures', 'review_commit_file_job-target_basic_result.html') }
      let(:rendered_output) { subject.rendered_file_content_template(File.read(markdown_filepath), default_data).gsub(/\s+/, ' ').strip }
      let(:default_data) do
        {
          'repository_url' => 'https://github.com/convinceme/arguments_for',
          'uri_escaped_page_name' => 'Test%20Topic.markdown'
        }
      end
      xit 'generates the output we want' do
        expect(text_without_citations(rendered_output)).to eq text_without_citations(target_output)

        target_ref_links = ref_links(target_output)
        ref_links(rendered_output).each_with_index do |ref, i|
          expect(ref).to eq(target_ref_links[i])
        end

        expect(link_to_pr(rendered_output)).to eq link_to_pr(target_output)

        target_references_section = references_section(target_output).split('</span>')
        references_section(rendered_output).split('</span>').each_with_index do |citation, i|
          expect(citation).to eq(target_references_section[i])
        end

        expect(character_count(rendered_output)).to eq character_count(target_output)

        expect(rendered_output).to eq target_output
      end
      xit 'sets the class on the article' do
        expect(subject).to receive(:default_data_for) { default_data }
        rendered_template = subject.rendered_template(CommitReview.new, 'Test Topic.markdown', File.read(markdown_filepath)).gsub(/\s+/, ' ').strip
        character_count_number = character_count(rendered_template)[/\d+/]
        expect(character_count_number).to eq '1518'
        article_tag = rendered_template[/<article[^>]*>/i]
        expect(article_tag).to match 'character-count-over'
      end
    end

    describe 'render actual thing' do
      let(:file_hash) do
        {
          'sha' => '9805e67acac3072c5a2db7985268d58c6579b372',
          'filename' => 'Outlaw the Death Penalty.markdown',
          'status' => 'renamed',
          'additions' => 0,
          'deletions' => 0,
          'changes' => 0,
          'blob_url' => 'https://github.com/convinceme/arguments_against/blob/f7f3630e40e867fb4fb4c2a94d170b4af98bab50/Outlaw%20the%20Death%20Penalty.markdown',
          'raw_url' => 'https://github.com/convinceme/arguments_against/raw/f7f3630e40e867fb4fb4c2a94d170b4af98bab50/Outlaw%20the%20Death%20Penalty.markdown',
          'contents_url' => 'https://api.github.com/repos/convinceme/arguments_against/contents/Outlaw%20the%20Death%20Penalty.markdown?ref=f7f3630e40e867fb4fb4c2a94d170b4af98bab50',
          'previous_filename' => 'Outlawing the Death Penalty.markdown'
        }
      end
      let(:commit_review) do
        FactoryGirl.create(:commit_review,
                           sha: 'f7f3630e40e867fb4fb4c2a94d170b4af98bab50',
                           repository: 'arguments_against',
                           pr_number: 17,
                           changed_files: { files: [file_hash] })
      end
      let(:rendered_output) { File.read(commit_review.output_filepath).gsub(/\s+/, ' ').strip }
      let(:target_filepath) { Rails.root.join('spec', 'fixtures', 'review_commit_file_job-target_full_result.html') }
      it 'renders' do
        commit_review.create_output_file

        VCR.use_cassette 'review_commit_file_job-actual-thing' do
          subject.perform(commit_review: commit_review, filename: file_hash['filename'])
        end

        target_ref_links = ref_links(target_output)
        ref_links(rendered_output).each_with_index do |ref, i|
          expect(ref).to eq(target_ref_links[i])
        end

        expect(link_to_pr(rendered_output)).to eq link_to_pr(target_output)

        target_references_section = references_section(target_output).split('</span>')
        references_section(rendered_output).split('</span>').each_with_index do |citation, i|
          expect(citation).to eq(target_references_section[i])
        end

        expect(character_count(rendered_output)).to eq character_count(target_output)
        expect(rendered_output).to eq target_output

        article_tag = rendered_output[/<article[^>]*>/i]
        expect(article_tag).to_not match 'character-count-over'
      end
    end
  end

  def text_section(content)
    content
      .gsub(/.*<section class..text[^>]*>/m, '')
      .gsub(/<\/section>.*/m, '')
  end

  def ref_links(content)
    text_section(content).split(/<sup/)
                         .select { |m| m =~ /<\/sup>/ }
                         .map { |m| m.gsub(/<.sup>.*/m, '') }
                         .map { |m| "<sup#{m}</sup>" }
  end

  def text_without_citations(content)
    text_section(content).split(/<sup/)
                         .map { |m| m.gsub(/.*<.sup>/m, '') }
                         .join
  end

  def link_to_pr(content)
    content
      .gsub(/.*<div class="link-to-pr"[^>]*>/m, '')
      .gsub(/<\/div>.*/m, '').strip
  end

  def character_count(content)
    content
      .gsub(/.*<div class="character-count"[^>]*>/m, '')
      .gsub(/<\/div>.*/m, '')
      .gsub(/<strong[^<]*/i, '')
      .gsub(/<\/strong>/i, '').strip
  end

  def references_section(content)
    content
      .gsub(/.*<section class..references[^>]*>/m, '')
      .gsub(/<\/section>.*/m, '')
  end
end
