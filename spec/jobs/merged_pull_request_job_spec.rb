# frozen_string_literal: true
require 'rails_helper'

RSpec.describe MergedPullRequestJob, type: :job do
  let(:subject) { MergedPullRequestJob.new }
  let(:api_connection) do
    GithubApi.new
             .pull_request_connection(repository: repository,
                                      pr_number: pr_number)
  end

  describe 'perform' do
    before do
      expect_any_instance_of(Faraday::Connection).to receive(:get).at_least(1).times
      ActiveJob::Base.queue_adapter = :test
    end
    context 'unmerged pull request' do
      before { expect(subject).to receive(:merged?) { false } }
      let(:repository) { 'blerg' }
      let(:pr_number) { 12 }
      it 'does nothing' do
        # Eventually, we'll be doing CI style posting of character count on PRs and stuff
        # For now, not doing anything
        expect do
          subject.perform(repository: repository, pr_number: pr_number)
        end.to change(ActiveJob::Base.queue_adapter.enqueued_jobs, :size).by 0
      end
    end
    context 'merged pull request' do
      before { expect(subject).to receive(:merged?) { true } }

      context 'info repo' do
        it 'calls update_categories_and_topics' do
          expect do
            subject.perform(repository: 'info', pr_number: 12)
          end.to have_enqueued_job(UpdateCategoriesAndTopicsJob)
        end
      end
      context 'unknown repository' do
        it 'does nothing' do
          expect do
            subject.perform(repository: 'blearg', pr_number: 12)
          end.to change(ActiveJob::Base.queue_adapter.enqueued_jobs, :size).by 0
        end
      end
      context 'perspective repo' do
        let(:perspective) { FactoryGirl.create(:perspective, name: 'Arguments For') }
        let(:argument) { FactoryGirl.create(:argument, perspective: perspective) }
        let!(:topic) { argument.topic }
        it 'calls update_argument_job for each modified topic' do
          expect(subject).to receive(:modified_topic_ids) { [topic.id] }
          expect do
            subject.perform(repository: perspective.repository, pr_number: 2)
          end.to have_enqueued_job(UpdateArgumentJob).with(argument)
        end
      end
    end
  end

  describe 'pull_request_files' do
    let(:repository) { 'arguments_for' }
    let(:pr_number) { 6 }
    let(:target_filenames) do
      [
        'Raise Minimum Wage to $15 an Hour.markdown'
      ]
    end
    it 'gets the files' do
      VCR.use_cassette 'merged_pull_request_job-pull_request_files' do
        expect(subject.pull_request_files(api_connection.get('files')))
          .to eq target_filenames
      end
    end
  end

  describe 'merged?' do
    context 'merged pull request' do
      let(:repository) { 'info' }
      let(:pr_number) { 3 }
      it 'returns true' do
        VCR.use_cassette 'merged_pull_request_job-merged_success' do
          expect(subject.merged?(api_connection.get('merge'))).to be_truthy
        end
      end
    end
    context 'unmerged pull request' do
      let(:repository) { 'info' }
      let(:pr_number) { 18 }
      it 'returns false' do
        VCR.use_cassette 'merged_pull_request_job-merged_fail' do
          expect(subject.merged?(api_connection.get('merge'))).to be_falsey
        end
      end
    end
  end

  describe 'modified_topic_ids' do
    let(:filenames) do
      [
        'Capital Punishment.MARKDOWN',
        'Private Prisons.md',
        'README.markdown'
      ]
    end
    let(:capital_punishment) { FactoryGirl.create(:topic, name: 'Capital Punishment') }
    let(:private_prisons) { FactoryGirl.create(:topic, name: 'Private Prisons') }
    let!(:target) { [capital_punishment.id, private_prisons.id] }
    it 'returns the modified topic ids' do
      expect(subject).to receive(:pull_request_files) { filenames }
      expect(subject.modified_topic_ids(nil)).to eq(target)
    end
  end
end
