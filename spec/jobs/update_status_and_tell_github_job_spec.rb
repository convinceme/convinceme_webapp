# frozen_string_literal: true
require 'rails_helper'

RSpec.describe UpdateStatusAndTellGithubJob, type: :job do
  let(:subject) { UpdateStatusAndTellGithubJob.new }
  let(:commit_review) { FactoryGirl.create(:commit_review, status: 'pending') }
  context 'not all reviewed' do
    it 'returns, does not set status' do
      expect(commit_review).to receive(:all_files_reviewed?) { false }
      subject.perform(commit_review)
      commit_review.reload
      expect(commit_review.status).to eq 'pending'
    end
  end
  describe 'all reviewed' do
    before { expect_any_instance_of(GithubApi).to receive(:update_pr_status) { true } }
    context 'various illegalities' do
      before do
        expect(commit_review).to receive(:all_files_reviewed?) { true }
        expect(commit_review).to receive(:filenames) { filenames }
        expect(subject).to receive(:file_reviews_content) { file_reviews_content }
        subject.perform(commit_review)
        # commit_review.reload
        expect(commit_review.status).to eq('failure')
      end
      context 'image present' do
        let(:file_reviews_content) { "<img src='lol'>" }
        let(:filenames) { [] }
        it 'sets failed status and status_details' do
          expect(commit_review.status_details).to match(/image/i)
        end
      end
      context 'article tag present' do
        let(:file_reviews_content) do
          '<article><section class="text">' \
          '<p>Proper justice insists upon a punishment that' \
          'befits the crime committed. Though severe, the' \
          '</section><section class="references">' \
          '<ol><li id="cite_note-retribution-(in-support-of-the-death-penalty)">' \
          '<span class="cite-backlink"><b><a href="#cite_ref-1">^</a></b></span>' \
          '<span class="reference-text"><cite><a href="http://dd.com/sss.htm">blah</a>' \
          '</cite></article>'
        end
        let(:filenames) { ['one filename.markdown'] }
        xit 'sets failed status, status_details' do
          expect(commit_review.status_details).to match(/article/i)
          expect(commit_review.status_details).to_not match(/link/i)
        end
      end
      context 'non-internal inline link' do
        let(:filenames) { [] }
        let(:file_reviews_content) do
          '<section class="text">' \
          '<p>Proper justice insists upon a punishment that' \
          'befits the crime committed. Though severe, the' \
          'death penalty is, <a href="https://malicious.org">times</a>'
        end
        it 'sets failed status'
      end
      context 'article, section, image non-internal link' do
        let(:filenames) { [] }
        let(:file_reviews_content) do
          "<article></article><section><img src='lol'>"
          # "<a href='https://malicious.com'></a></section>"
        end
        xit 'sets failed status, includes all the things' do
          expect(commit_review.status_details).to match(/article/i)
          expect(commit_review.status_details).to match(/section/i)
          expect(commit_review.status_details).to match(/image/i)
          # expect(commit_review.status_details).to match(/link/i)
        end
      end
      context 'over char count' do
        let(:filenames) { ['Test Topic.markdown'] }
        let(:file_reviews_content) do
          '<article data-filename="Test Topic.markdown" class="reviewed-file character-count-over"> <div class="argument"> <h2>Test Topic.markdown</h2> <div class="content-inner">'
        end
        it 'sets failing status' do
          expect(commit_review.status_details).to match(/long/i)
        end
      end
    end
    context 'all under char limit, reference link, actual test' do

    end
  end
end
