# frozen_string_literal: true
require 'rails_helper'

RSpec.describe ReviewCommitJob, type: :job do
  let(:subject) { ReviewCommitJob }
  let(:instance) { subject.new }

  describe 'perform' do
    context 'closed pr' do
      xit 'does not review' do
        expect(instance.perform(repository: 'asdf', sha: 'adsf', pr_number: 12, pr_action: 'closed'))
          .to eq 'Not reviewed action'
      end
    end
    context 'open pr' do
      before { expect_any_instance_of(GithubApi).to receive(:update_pr_status) }
      let!(:perspective) { FactoryGirl.create(:perspective) }
      let(:repository) { perspective.repository }
      context 'non-existant commit review' do
        before do
          expect(instance).to receive(:fetch_changed_files) { [] }
        end
        xit 'creates the commit review' do
          expect do
            instance.perform(repository: repository, sha: 'adsf', pr_number: 12, pr_action: 'open')
          end.to be_changed(CommitReview, :count).by 1
          commit_review = CommitReview.last
          expect(commit_review.output_filepath.exist?).to be_truthy
          expect(commit_review.status).to eq 'pending'
        end
      end
      context 'existing commit review' do
        let(:filename) { 'Filename 1.md' }
        let!(:commit_review) { FactoryGirl.create(:commit_review, repository: repository, pr_number: 12, sha: 'adsf', status: 'failure') }
        before do
          expect_any_instance_of(CommitReview).to receive(:filenames).at_least(1).times { [filename] }
          ActiveJob::Base.queue_adapter = :test
        end
        xit 'enqueues with the filename' do
          # Note - this doesn't create a file, because that is created in the creation step
          initial_commit_review_count = CommitReview.count
          expect do
            instance.perform(repository: repository, sha: 'adsf', pr_number: 12, pr_action: 'open')
          end.to have_enqueued_job(ReviewCommitFileJob).with(commit_review: commit_review, filename: filename)
          expect(CommitReview.count).to eq initial_commit_review_count
          commit_review.reload
          expect(commit_review.status).to eq 'failure' # ensure we don't reset the status
        end
      end
    end
  end

  describe 'store_changed_files' do
    let(:sha) { 'f7f3630e40e867fb4fb4c2a94d170b4af98bab50' }
    let(:repository) { 'arguments_against' }
    let(:pr_number) { 17 }
    let(:target_files_changed) do
      [
        {
          'sha' => '267a515f1d7da0fbe6306f7d6bf0b9e4800b0903',
          'filename' => 'Create a Carbon Tax.markdown',
          'status' => 'renamed',
          'additions' => 0,
          'deletions' => 0,
          'changes' => 0,
          'blob_url' => 'https://github.com/convinceme/arguments_against/blob/f7f3630e40e867fb4fb4c2a94d170b4af98bab50/Create%20a%20Carbon%20Tax.markdown',
          'raw_url' => 'https://github.com/convinceme/arguments_against/raw/f7f3630e40e867fb4fb4c2a94d170b4af98bab50/Create%20a%20Carbon%20Tax.markdown',
          'contents_url' => 'https://api.github.com/repos/convinceme/arguments_against/contents/Create%20a%20Carbon%20Tax.markdown?ref=f7f3630e40e867fb4fb4c2a94d170b4af98bab50',
          'previous_filename' => 'Creating a Carbon Tax.markdown'
        },
        {
          'sha' => '7ae94986b3ab6392df1619173e55fa4baa75f76c',
          'filename' => 'Decriminalize Marijuana Use.markdown',
          'status' => 'renamed',
          'additions' => 0,
          'deletions' => 0,
          'changes' => 0,
          'blob_url' => 'https://github.com/convinceme/arguments_against/blob/f7f3630e40e867fb4fb4c2a94d170b4af98bab50/Decriminalize%20Marijuana%20Use.markdown',
          'raw_url' => 'https://github.com/convinceme/arguments_against/raw/f7f3630e40e867fb4fb4c2a94d170b4af98bab50/Decriminalize%20Marijuana%20Use.markdown',
          'contents_url' => 'https://api.github.com/repos/convinceme/arguments_against/contents/Decriminalize%20Marijuana%20Use.markdown?ref=f7f3630e40e867fb4fb4c2a94d170b4af98bab50',
          'previous_filename' => 'Decriminalizing Marijuana Use.markdown'
        },
        {
          'sha' => '9805e67acac3072c5a2db7985268d58c6579b372',
          'filename' => 'Outlaw the Death Penalty.markdown',
          'status' => 'renamed',
          'additions' => 0,
          'deletions' => 0,
          'changes' => 0,
          'blob_url' => 'https://github.com/convinceme/arguments_against/blob/f7f3630e40e867fb4fb4c2a94d170b4af98bab50/Outlaw%20the%20Death%20Penalty.markdown',
          'raw_url' => 'https://github.com/convinceme/arguments_against/raw/f7f3630e40e867fb4fb4c2a94d170b4af98bab50/Outlaw%20the%20Death%20Penalty.markdown',
          'contents_url' => 'https://api.github.com/repos/convinceme/arguments_against/contents/Outlaw%20the%20Death%20Penalty.markdown?ref=f7f3630e40e867fb4fb4c2a94d170b4af98bab50',
          'previous_filename' => 'Outlawing the Death Penalty.markdown'
        },
        {
          'sha' => '4b75050a3b0c1c5d5158903d06bba94c8d7cfd38',
          'filename' => 'Penalize Sanctuary Cities.markdown',
          'status' => 'renamed',
          'additions' => 0,
          'deletions' => 0,
          'changes' => 0,
          'blob_url' => 'https://github.com/convinceme/arguments_against/blob/f7f3630e40e867fb4fb4c2a94d170b4af98bab50/Penalize%20Sanctuary%20Cities.markdown',
          'raw_url' => 'https://github.com/convinceme/arguments_against/raw/f7f3630e40e867fb4fb4c2a94d170b4af98bab50/Penalize%20Sanctuary%20Cities.markdown',
          'contents_url' => 'https://api.github.com/repos/convinceme/arguments_against/contents/Penalize%20Sanctuary%20Cities.markdown?ref=f7f3630e40e867fb4fb4c2a94d170b4af98bab50',
          'previous_filename' => 'Penalizing Sanctuary Cities.markdown'
        },
        {
          'sha' => 'ed724cf08d2222157fb279bac3ee74f99378d4c3',
          'filename' => 'Police With Stop and Frisk.markdown',
          'status' => 'renamed',
          'additions' => 0,
          'deletions' => 0,
          'changes' => 0,
          'blob_url' => 'https://github.com/convinceme/arguments_against/blob/f7f3630e40e867fb4fb4c2a94d170b4af98bab50/Police%20With%20Stop%20and%20Frisk.markdown',
          'raw_url' => 'https://github.com/convinceme/arguments_against/raw/f7f3630e40e867fb4fb4c2a94d170b4af98bab50/Police%20With%20Stop%20and%20Frisk.markdown',
          'contents_url' => 'https://api.github.com/repos/convinceme/arguments_against/contents/Police%20With%20Stop%20and%20Frisk.markdown?ref=f7f3630e40e867fb4fb4c2a94d170b4af98bab50',
          'previous_filename' => 'Policing With Stop and Frisk.markdown'
        },
        {
          'sha' => 'e249ccce2aa4e91e12343bfacb70e07201539534',
          'filename' => 'Prevent the Dakota Access Pipeline (DAPL).markdown',
          'status' => 'renamed',
          'additions' => 0,
          'deletions' => 0,
          'changes' => 0,
          'blob_url' => 'https://github.com/convinceme/arguments_against/blob/f7f3630e40e867fb4fb4c2a94d170b4af98bab50/Prevent%20the%20Dakota%20Access%20Pipeline%20(DAPL).markdown',
          'raw_url' => 'https://github.com/convinceme/arguments_against/raw/f7f3630e40e867fb4fb4c2a94d170b4af98bab50/Prevent%20the%20Dakota%20Access%20Pipeline%20(DAPL).markdown',
          'contents_url' => 'https://api.github.com/repos/convinceme/arguments_against/contents/Prevent%20the%20Dakota%20Access%20Pipeline%20(DAPL).markdown?ref=f7f3630e40e867fb4fb4c2a94d170b4af98bab50',
          'previous_filename' => 'Preventing the Dakota Access Pipeline (DAPL).markdown'
        },
        {
          'sha' => '17ac9e4dac1cb326ad5b7e9d7bebe28f133e0f39',
          'filename' => 'Provide School Vouchers.markdown',
          'status' => 'renamed',
          'additions' => 0,
          'deletions' => 0,
          'changes' => 0,
          'blob_url' => 'https://github.com/convinceme/arguments_against/blob/f7f3630e40e867fb4fb4c2a94d170b4af98bab50/Provide%20School%20Vouchers.markdown',
          'raw_url' => 'https://github.com/convinceme/arguments_against/raw/f7f3630e40e867fb4fb4c2a94d170b4af98bab50/Provide%20School%20Vouchers.markdown',
          'contents_url' => 'https://api.github.com/repos/convinceme/arguments_against/contents/Provide%20School%20Vouchers.markdown?ref=f7f3630e40e867fb4fb4c2a94d170b4af98bab50',
          'previous_filename' => 'Providing School Vouchers.markdown'
        },
        {
          'sha' => 'c0f34180c884d663a8de051b805d1ef87574dce4',
          'filename' => 'Raise Minimum Wage to $15 an Hour.markdown',
          'status' => 'renamed',
          'additions' => 0,
          'deletions' => 0,
          'changes' => 0,
          'blob_url' => 'https://github.com/convinceme/arguments_against/blob/f7f3630e40e867fb4fb4c2a94d170b4af98bab50/Raise%20Minimum%20Wage%20to%20$15%20an%20Hour.markdown',
          'raw_url' => 'https://github.com/convinceme/arguments_against/raw/f7f3630e40e867fb4fb4c2a94d170b4af98bab50/Raise%20Minimum%20Wage%20to%20$15%20an%20Hour.markdown',
          'contents_url' => 'https://api.github.com/repos/convinceme/arguments_against/contents/Raise%20Minimum%20Wage%20to%20$15%20an%20Hour.markdown?ref=f7f3630e40e867fb4fb4c2a94d170b4af98bab50',
          'previous_filename' => 'Raising Minimum Wage to $15 an Hour.markdown'
        },
        {
          'sha' => '91721512adf448eddd6bdfa232eeee9d0c5a1e3f',
          'filename' => 'Require Abstinence-Only Education.markdown',
          'status' => 'renamed',
          'additions' => 0,
          'deletions' => 0,
          'changes' => 0,
          'blob_url' => 'https://github.com/convinceme/arguments_against/blob/f7f3630e40e867fb4fb4c2a94d170b4af98bab50/Require%20Abstinence-Only%20Education.markdown',
          'raw_url' => 'https://github.com/convinceme/arguments_against/raw/f7f3630e40e867fb4fb4c2a94d170b4af98bab50/Require%20Abstinence-Only%20Education.markdown',
          'contents_url' => 'https://api.github.com/repos/convinceme/arguments_against/contents/Require%20Abstinence-Only%20Education.markdown?ref=f7f3630e40e867fb4fb4c2a94d170b4af98bab50',
          'previous_filename' => 'Requiring Abstinence-Only Education.markdown'
        },
        {
          'sha' => '90f1affd4c6404703d3bd734b65464524f0968ba',
          'filename' => 'Require Background Checks on All Gun Purchases.markdown',
          'status' => 'renamed',
          'additions' => 0,
          'deletions' => 0,
          'changes' => 0,
          'blob_url' => 'https://github.com/convinceme/arguments_against/blob/f7f3630e40e867fb4fb4c2a94d170b4af98bab50/Require%20Background%20Checks%20on%20All%20Gun%20Purchases.markdown',
          'raw_url' => 'https://github.com/convinceme/arguments_against/raw/f7f3630e40e867fb4fb4c2a94d170b4af98bab50/Require%20Background%20Checks%20on%20All%20Gun%20Purchases.markdown',
          'contents_url' => 'https://api.github.com/repos/convinceme/arguments_against/contents/Require%20Background%20Checks%20on%20All%20Gun%20Purchases.markdown?ref=f7f3630e40e867fb4fb4c2a94d170b4af98bab50',
          'previous_filename' => 'Requiring Background Checks on All Gun Purchases.markdown'
        },
        {
          'sha' => '1e18e3f3c28826a5cd53131ef823ed0a225ebb2c',
          'filename' => 'Require Free Tuition at Public Colleges and Universities.markdown',
          'status' => 'renamed',
          'additions' => 0,
          'deletions' => 0,
          'changes' => 0,
          'blob_url' => 'https://github.com/convinceme/arguments_against/blob/f7f3630e40e867fb4fb4c2a94d170b4af98bab50/Require%20Free%20Tuition%20at%20Public%20Colleges%20and%20Universities.markdown',
          'raw_url' => 'https://github.com/convinceme/arguments_against/raw/f7f3630e40e867fb4fb4c2a94d170b4af98bab50/Require%20Free%20Tuition%20at%20Public%20Colleges%20and%20Universities.markdown',
          'contents_url' => 'https://api.github.com/repos/convinceme/arguments_against/contents/Require%20Free%20Tuition%20at%20Public%20Colleges%20and%20Universities.markdown?ref=f7f3630e40e867fb4fb4c2a94d170b4af98bab50',
          'previous_filename' => 'Requiring Free Tuition at Public Colleges and Universities.markdown'
        }
      ]
    end
    let(:commit_review) { FactoryGirl.create(:commit_review, sha: sha, pr_number: pr_number, repository: repository) }
    xit 'gets the list that we want' do
      VCR.use_cassette 'review_commit_job-store_changed_files' do
        expect(instance.store_changed_files(commit_review)).to eq target_files_changed.map { |f| f['filename'] }
      end
      commit_review.reload
      expect(commit_review.changed_files['files']).to eq(target_files_changed)
    end
  end
end
