# frozen_string_literal: true
require 'rails_helper'

RSpec.describe UpdateArgumentJob, type: :job do
  let(:subject) { UpdateArgumentJob.new }
  let(:topic) { FactoryGirl.create(:topic, name: 'Requiring Abstinence-Only Education', updated_at: last_updated_at) }
  let(:perspective) { FactoryGirl.create(:perspective, name: 'Arguments Against') }
  let(:last_updated_at) { Time.now - 1.day }
  let(:argument) do
    FactoryGirl.create(:argument,
                       topic: topic,
                       updated_at: last_updated_at,
                       body_html: '<div>Initial</div>')
  end

  context 'stubbed http connections' do
    context 'page not on github.io' do
      it 'updates checked_github_at, github_updated_at and empties body' do
        expect(argument.updated_at).to be_within(1.seconds).of last_updated_at
        expect(topic.updated_at).to be_within(1.seconds).of last_updated_at
        expect(subject).to receive(:github_pages_html) { nil }
        subject.perform(argument)

        argument.reload
        topic.reload
        expect(argument.updated_at).to be_within(1.seconds).of(Time.now)
        expect(topic.updated_at).to be_within(1.seconds).of(Time.now)
        expect(argument.body_html).to be_nil
      end
    end
    context 'github not updated since checked' do
      it 'updates' do
        expect(subject).to receive(:github_pages_html) { '<div>Initial</div>' }
        subject.perform(argument)

        argument.reload
        topic.reload
        expect(argument.updated_at).to be_within(1.seconds).of last_updated_at
        expect(topic.updated_at).to be_within(1.seconds).of last_updated_at
      end
    end
    context 'github updated' do
      let(:target_html) { '<div>New html</div>' }
      it 'works' do
        expect(subject).to receive(:github_pages_html) { target_html }
        subject.perform(argument)

        argument.reload
        expect(argument.body_html).to eq target_html
      end
    end
  end

  describe 'github_pages_html' do
    context 'existing page' do
      let(:url) { 'https://convinceme.github.io/arguments_for/Require%20Abstinence-Only%20Education' }
      it 'returns content' do
        VCR.use_cassette 'update_argument_job-existing' do
          result = subject.github_pages_html(url)
          expect(result).to match(/<section class="text">/)
        end
      end
    end
    context 'non-existing page' do
      let(:url) { 'https://convinceme.github.io/content_from_conservative/unknown%20page' }
      it 'returns nil' do
        VCR.use_cassette 'update_argument_job-non_existant_page' do
          expect(subject.github_pages_html(url)).to eq nil
        end
      end
    end
  end
end
