# frozen_string_literal: true
require 'rails_helper'

RSpec.describe UpdateCategoriesAndTopicsJob, type: :job do
  let(:subject) { UpdateCategoriesAndTopicsJob.new }
  let(:first_topic) do
    {
      name: 'Require Abstinence-Only Education',
      category_names: %w(Health Sex Education),
      parent_topic_id: nil
    }
  end
  let(:first_category) { { name: 'Health' } }

  context 'Post tsv downloading and processing' do
    let(:processed_topics) do
      [
        first_topic,
        {
          name: 'Prevent the Dakota Access Pipeline (DAPL)',
          category_names: %w(Environment),
          parent_topic_id: nil
        }
      ]
    end
    let(:processed_categories) do
      [
        first_category,
        { name: 'Sex' },
        { name: 'Education' },
        { name: 'environment' }
      ]
    end
    let(:environment) { FactoryGirl.create(:category, name: 'Environment') }
    let(:initial_category) { FactoryGirl.create(:category, name: 'HeaLTh') }
    let(:initial_topic) { FactoryGirl.create(:topic, name: 'Require Abstinence ONLY Education') }
    let(:renamed_topic) { FactoryGirl.create(:topic, name: 'Block Dakota Access Pipeline (DAPL)') }
    let!(:perspective_1) { FactoryGirl.create(:perspective, name: 'blargh') }
    let!(:perspective_2) { FactoryGirl.create(:perspective, name: 'bloop') }
    before do
      # Rename the topic!
      renamed_topic.update_attributes(name: 'Prevent the Dakota Access Pipeline (DAPL)')
      FactoryGirl.create(:topic_category, category: initial_category, topic: initial_topic)
      FactoryGirl.create(:topic_category, category: environment, topic: initial_topic)
      FactoryGirl.create(:argument, perspective: perspective_1, topic: initial_topic)
    end
    it 'creates the  the things that we want it to call' do
      expect(subject).to receive(:processed_categories) { processed_categories }
      expect(subject).to receive(:processed_topics) { processed_topics }
      subject.perform

      health = Category.friendly_find initial_category.id
      sex = Category.friendly_find 'sex '
      education = Category.friendly_find 'Education'
      expect(Category.friendly_find('environment')).to eq environment # test that we don't topic unnecessarily

      abstinence_only = Topic.friendly_find(initial_topic.id)
      expect(abstinence_only.name).to eq 'Require Abstinence-Only Education' # Test capitalization fix
      expect(abstinence_only.categories.pluck(:id)).to eq([health.id, sex.id, education.id])

      expect(renamed_topic.categories.pluck(:id)).to eq([environment.id])

      Topic.pluck(:id).each do |topic_id|
        expect(perspective_1.arguments.where(topic_id: topic_id).count).to eq 1
        expect(perspective_2.arguments.where(topic_id: topic_id).count).to eq 1
      end
    end
  end

  it 'processes_topics' do
    parent_topic = FactoryGirl.create(:topic, name: 'Restrict Sex Education')
    row = {
      topic: 'Require Abstinence-Only Education',
      categories: ' Health, , Sex, Education ',
      parent_topic: ' Restrict Sex Education' # idk, some category
    }
    expect(subject.process_topic(row)).to eq first_topic.merge(parent_topic_id: parent_topic.id)
  end

  # it 'successfully processes topics' do
  #   VCR.use_cassette 'update_categories_and_topics_job-topics_tsv' do
  #     processed_topics = subject.processed_topics
  #     expect(processed_topics.count).to be >= 2
  #     expect(processed_topics.first).to eq first_topic
  #   end
  # end

  it 'successfully processes categories' do
    target_first_category = { name: 'Campaign Finance' }
    VCR.use_cassette 'update_categories_and_topics_job-categories_tsv' do
      processed_categories = subject.processed_categories
      expect(processed_categories.count).to be > 4
      expect(processed_categories.first).to eq target_first_category
    end
  end
end
