# frozen_string_literal: true
FactoryGirl.define do
  factory :user do
    sequence(:email) { |n| "stuff#{n}@example.com" }
    password 'something1212'
  end
end
