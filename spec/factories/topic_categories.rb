# frozen_string_literal: true
FactoryGirl.define do
  factory :topic_category do
    topic { FactoryGirl.create(:topic) }
    category { FactoryGirl.create(:category) }
  end
end
