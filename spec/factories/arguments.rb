# frozen_string_literal: true
FactoryGirl.define do
  factory :argument do
    topic { FactoryGirl.create(:topic) }
    perspective { FactoryGirl.create(:perspective) }
    sequence(:body_html) { |n| '<section class=\"text\"><p>Some text or something</p></section>' }
  end
end
