# frozen_string_literal: true
FactoryGirl.define do
  factory :perspective do
    sequence(:name) { |n| "Perspective #{n}" }
  end
end
