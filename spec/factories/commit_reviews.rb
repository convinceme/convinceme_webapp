# frozen_string_literal: true
FactoryGirl.define do
  factory :commit_review do
    sequence(:sha) { |n| "#{n}tcxasdf98vxcva" }
    repository { 'arguments_for' }
    status 'pending'
  end
end
