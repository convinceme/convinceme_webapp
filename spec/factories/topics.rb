# frozen_string_literal: true
FactoryGirl.define do
  factory :topic do
    sequence(:name) { |n| "A Government action #{n}" }
  end
end
