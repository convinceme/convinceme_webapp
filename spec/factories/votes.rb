# frozen_string_literal: true
FactoryGirl.define do
  factory :vote do
    argument { FactoryGirl.create(:argument) }
    user { FactoryGirl.create(:user) }
    kind 'opinion'
  end
end
