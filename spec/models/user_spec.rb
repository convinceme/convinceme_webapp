# frozen_string_literal: true
require 'rails_helper'

RSpec.describe User, type: :model do
  describe 'can_vote' do
    let(:user) { FactoryGirl.create(:user) }
    let(:topic) { FactoryGirl.create(:topic) }
    let(:argument) { FactoryGirl.create(:argument, topic: topic) }
    it 'returns false if no topic present' do
      expect(user.can_vote?(kind: 'quality', topic: nil)).to be_falsey
    end
    it 'returns false if non vote kind' do
      expect(user.can_vote?(kind: 'blarp', topic: nil)).to be_falsey
    end
    it 'returns true if no votes' do
      expect(user.can_vote?(kind: 'quality', topic: topic)).to be_truthy
    end
    it 'returns true if other kind of vote voted' do
      FactoryGirl.create(:vote, user: user, kind: 'opinion', argument: argument)
      expect(user.can_vote?(kind: 'quality', topic: topic)).to be_truthy
    end
    it 'returns false if vote happened since topic was updated' do
      FactoryGirl.create(:vote, user: user, kind: 'opinion', argument: argument)
      expect(user.can_vote?(kind: 'opinion', topic: topic)).to be_falsey
    end
    context 'vote before topic updated' do
      let(:first_vote) { FactoryGirl.create(:vote, user: user, kind: 'opinion', argument: argument) }
      before do
        first_vote.update_attribute :created_at, Time.now - 2.hours
        expect(first_vote.created_at).to be < topic.updated_at
      end
      it 'returns true if the vote happened before topic updated' do
        expect(user.can_vote?(kind: 'opinion', topic: topic)).to be_truthy
      end
      it 'returns false if there is another vote after topic updated' do
        FactoryGirl.create(:vote, user: user, kind: 'opinion', argument: argument)
        expect(user.can_vote?(kind: 'opinion', topic: topic)).to be_falsey
      end
    end
  end
end
