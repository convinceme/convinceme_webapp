# frozen_string_literal: true
require 'rails_helper'

RSpec.describe Vote, type: :model do
  describe 'kinds' do
    let(:target_msg) { 'Kind booger is not one of: opinion or quality' }
    it 'is a kind of vote we know' do
      vote = Vote.new(kind: 'booger')
      expect(vote.valid?).to be_falsey
      expect(vote.errors.full_messages.include?(target_msg)).to be_truthy
    end
  end
end
