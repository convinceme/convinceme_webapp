# frozen_string_literal: true
require 'rails_helper'

RSpec.describe Topic, type: :model do
  it_behaves_like 'sluggable'

  describe 'friendly_find' do
    let!(:topic) { FactoryGirl.create(:topic, name: 'new name', previous_slug: 'old_name') }
    it 'finds by the slug and the previous_slug' do
      expect(Topic.friendly_find(topic.slug)).to eq topic
      expect(Topic.friendly_find('Old+Name')).to eq topic
    end
  end

  describe 'set_previous_slug' do
    context 'unset' do
      let(:topic) { Topic.new(slug: 'something') }
      it 'sets' do
        topic.set_previous_slug
        expect(topic.previous_slug).to eq 'something'
      end
    end
    context 'set' do
      let(:topic) { Topic.new(slug: 'something', previous_slug: 'whooo') }
      it 'does not set' do
        topic.set_previous_slug
        expect(topic.previous_slug).to eq 'whooo'
      end
    end
    it 'has a before_save filter' do
      expect(Topic._save_callbacks.select { |cb| cb.kind.eql?(:before) }
        .map(&:raw_filter).include?(:set_previous_slug)).to eq(true)
    end
  end

  describe 'parent and child relationship' do
    let(:parent) { FactoryGirl.create(:topic) }
    let(:child) { FactoryGirl.create(:topic, parent_topic: parent) }
    it 'associates as expected' do
      expect(child.parent_topic).to eq parent
      expect(parent.child_topics).to eq([child])
    end
  end
end
