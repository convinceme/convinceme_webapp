# frozen_string_literal: true
require 'rails_helper'

RSpec.describe CommitReview, type: :model do
  describe 'output filepath' do
    it 'is the same for PRs with the same sha' do
      commit_review1 = CommitReview.new(sha: 'fffff99999', repository: 'arguments_for', pr_number: 12)
      commit_review2 = CommitReview.new(sha: 'fffff99999', repository: 'arguments_for', pr_number: 13)
      expect(commit_review1.output_filepath).to eq commit_review2.output_filepath
    end
  end

  context 'mini-sha' do
    let!(:commit_review) { FactoryGirl.create(:commit_review) }
    it 'finds by sha and short sha' do
      expect(CommitReview.friendly_find_by_sha(commit_review.sha)).to eq commit_review
      expect(CommitReview.friendly_find_by_sha(commit_review.short_sha)).to eq commit_review
    end
  end

  describe 'argument_filenames' do
    let(:commit_review) { CommitReview.new }
    let(:filenames) do
      [
        'Test Topic.md',
        'README.markdown',
        '.gitignore',
        '.gitmodules',
        'test',
        'Another Change File.MARKDOWN'
      ]
    end
    let(:target_argument_filenames) { ['Test Topic.md', 'Another Change File.MARKDOWN'] }
    it 'returns just the markdown filenames' do
      expect(commit_review).to receive(:filenames) { filenames }
      expect(commit_review.argument_filenames).to eq target_argument_filenames
    end
  end

  describe 'reviewed?' do
    let(:commit_review) { CommitReview.new }
    let(:output_filepath) { Rails.root.join('spec', 'fixtures', 'commit_review_output_file_with_reviewed_file.html') }
    before { expect(commit_review).to receive(:output_filepath).at_least(1).times { output_filepath } }
    let(:filename) { 'First File.markdown' }
    describe 'filename_reviewed?' do
      context 'file is written' do
        it 'is true' do
          expect(commit_review.filename_reviewed?(filename)).to be_truthy
        end
      end
      context 'file is not written' do
        let(:filename) { 'Another File.markdown' }
        it 'is false' do
          expect(commit_review.filename_reviewed?(filename)).to be_falsey
        end
      end
    end
    describe 'all_files_reviewed?' do
      before { expect(commit_review).to receive(:filenames) { filenames } }
      context 'both files reviewed' do
        let(:filenames) { ['Third File.markdown', 'First File.markdown'] }
        it 'is true' do
          expect(commit_review.all_files_reviewed?).to be_truthy
        end
      end
      context 'two of three files reviewed' do
        let(:filenames) { ['Third File.markdown', 'First File.markdown', 'Second File.markdown'] }
        it 'is true' do
          expect(commit_review.all_files_reviewed?).to be_falsey
        end
      end
    end
  end

  context '872958986a7750c88138da11009acd02fb72a466' do
    let(:sha) { '872958986a7750c88138da11009acd02fb72a466' }
    let(:changed_files) do
      '{
        "files": [
          {
            "sha": "872958986a7750c88138da11009acd02fb72a466",
            "filename": "Pull out of Paris Agreement.markdown",
            "status": "added",
            "additions": 19,
            "deletions": 0,
            "changes": 19,
            "blob_url": "https://github.com/convinceme/arguments_for/blob/5b7c745ea50c236d82f9125ec4ee691f4028dffe/Pull%20out%20of%20Paris%20Agreement.markdown",
            "raw_url": "https://github.com/convinceme/arguments_for/raw/5b7c745ea50c236d82f9125ec4ee691f4028dffe/Pull%20out%20of%20Paris%20Agreement.markdown",
            "contents_url": "https://api.github.com/repos/convinceme/arguments_for/contents/Pull%20out%20of%20Paris%20Agreement.markdown?ref=5b7c745ea50c236d82f9125ec4ee691f4028dffe",
            "patch": "@@ -0,0 +1,19 @@\n+---\n+references:\n+  -\n+    title: Paris Climate Agreement \'May Signal End of Fossil Fuel Era\'\n+    link: https://www.theguardian.com/environment/2015/dec/13/paris-climate-agreement-signal-end-of-fossil-fuel-era\n+    author: John Vidal\n+    publisher: The Guardian\n+    date_published: 2015-13-12\n+  -\n+    title: \n+    link: \n+    author: \n+    publisher: \n+    date_published: \n+---\n+\n+Climate change is the single largest threat facing humanity right now.\n+\n+Al Gore said of the Paris agreement, \"no agreement is perfect, and this one must be strengthened over time, but groups across every sector of society will now begin to reduce dangerous carbon pollution through the framework of this agreement.\"<ref>Paris Climate Agreement \'May Signal End of Fossil Fuel Era\'</ref>"
          }
        ]
      }'
    end
    let(:commit_review) do
      FactoryGirl.create(:commit_review,
                         sha: sha,
                         repository: 'arguments_for',
                         changed_files: JSON.parse(changed_files))
    end
    let(:filename) { 'Pull out of Paris Agreement.markdown' }
    describe 'raw_file_url' do
      let(:target_url) { 'https://github.com/convinceme/arguments_for/raw/5b7c745ea50c236d82f9125ec4ee691f4028dffe/Pull%20out%20of%20Paris%20Agreement.markdown' }
      it 'returns the raw_url' do
        expect(commit_review.raw_file_url(filename)).to eq target_url
      end
    end
    describe 'file_content' do
      let(:file_fixture) { Rails.root.join('spec', 'fixtures', 'commit_review_raw_file_content.markdown') }
      it 'returns the file content' do
        VCR.use_cassette 'commit_review-raw_file_content' do
          expect(commit_review.file_content(filename).strip).to eq File.read(file_fixture).strip
        end
      end
    end
  end
end
