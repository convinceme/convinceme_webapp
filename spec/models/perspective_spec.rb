# frozen_string_literal: true
require 'rails_helper'

RSpec.describe Perspective, type: :model do
  it_behaves_like 'sluggable'

  it 'returns the api url we want' do
    perspective = Perspective.new(repository: 'content_from_conservative')
    expect(perspective.api_url).to eq 'https://api.github.com/repos/convinceme/content_from_conservative'
  end

  it 'sets the default repository on create' do
    perspective = Perspective.create(name: 'Less Government')
    expect(perspective.repository).to eq 'less_government'
  end
end
