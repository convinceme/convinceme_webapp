# frozen_string_literal: true
require 'rails_helper'

RSpec.describe GithubApi, type: :model do
  it 'returns the api accept header we want' do
    expect(GithubApi.accept_header).to eq 'application/vnd.github.v3+json'
  end

  it 'returns the time at the timestamp' do
    result = GithubApi.parsed_timestamp('2017-02-13T06:55:45Z')
    expect(result.to_i).to eq 1486968945
  end
end
