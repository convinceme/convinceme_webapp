# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Argument, type: :model do
  it 'returns the github pages url we want' do
    perspective = Perspective.new(repository: 'content_from_conservative')
    topic = Topic.new(name: 'Things: Stuff Concerned')
    argument = Argument.new
    allow(argument).to receive(:perspective) { perspective }
    allow(argument).to receive(:topic) { topic }

    expect(argument.github_pages_url).to eq 'https://convinceme.github.io/content_from_conservative/Things:%20Stuff%20Concerned'
  end

  describe 'empty reference' do
    let(:empty_reference) { { title: nil, links: nil, author: nil, publisher: nil, date_published: nil } }
    it 'returns what we expect' do
      expect(Argument.new.empty_reference).to eq empty_reference
    end
  end
end
