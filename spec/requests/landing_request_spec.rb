# frozen_string_literal: true
require 'rails_helper'

RSpec.describe 'LandingController', type: :request do
  describe 'root' do
    let(:parent_topic) { FactoryGirl.create(:topic) }
    let!(:topic) { FactoryGirl.create(:topic, parent_topic: parent_topic) }
    it 'renders' do
      get '/'
      expect(response.code).to eq('200')
      expect(response).to render_template :home
      expect(response.body).to be_present
    end
  end
end
