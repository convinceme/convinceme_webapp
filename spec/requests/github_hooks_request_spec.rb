# frozen_string_literal: true
require 'rails_helper'

RSpec.describe 'GithubHooksController', type: :request do
  describe 'pull_request' do
    let(:github_hook_secret) { '78fdxc78v78zxcv' }
    let(:payload) do
      {
        action: 'opened', # or 'closed'
        number: 1,
        pull_request: {
          head: {
            sha: 'aaaaaaaaaaa8cx'
          }
        },
        repository: {
          id: 35129377,
          name: 'public-repo'
        }
      }
    end
    let(:post_headers) { { 'CONTENT_TYPE' => 'application/json' } }
    before do
      ActiveJob::Base.queue_adapter = :test
      ENV['GITHUB_HOOK_SECRET'] = github_hook_secret
    end
    context 'with webhook secret' do
      let(:target_args) { { repository: 'public-repo', pr_number: 1, pr_action: 'opened', sha: 'aaaaaaaaaaa8cx' }.with_indifferent_access }
      it 'calls check pull request job with the correct parameters' do
        expect do
          post '/github_webhook_pull_request', params: payload.to_json,
                                               headers: post_headers.merge('HTTP_X_HUB_SIGNATURE' => 'sha1=29eb112105405c18058652aa5b165eeb86e76736')
          expect(response.code).to eq('204')
        end.to change(ActiveJob::Base.queue_adapter.enqueued_jobs, :size).by 3
        arguments = ActiveJob::Base.queue_adapter.enqueued_jobs.first[:args]
          .first.except('_aj_symbol_keys')
        expect(arguments).to eq(target_args)
      end
    end
    context 'not a pull_request' do
      it 'responds with not a pull request' do
        expect do
          post '/github_webhook_pull_request', params: payload.except(:pull_request).to_json,
                                               headers: post_headers.merge('HTTP_X_HUB_SIGNATURE' => 'sha1=6da6a2e41d7eaaa31f5037ead006919597462207')
          expect(response.code).to eq('204')
        end.to change(ActiveJob::Base.queue_adapter.enqueued_jobs, :size).by 0
      end
    end
    context 'without webhook secret' do
      it 'responds with 500' do
        expect do
          post '/github_webhook_pull_request', params: payload.to_json,
                                               headers: post_headers.merge('HTTP_X_HUB_SIGNATURE' => 'asd')
        end.to raise_error("Signatures didn't match!")
      end
    end
  end
end
