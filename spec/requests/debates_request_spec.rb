# frozen_string_literal: true

require 'rails_helper'

base_url = '/debates'
RSpec.describe 'DebatesController', type: :request do
  let(:body_1) { 'party body' }
  let(:topic) { FactoryGirl.create(:topic) }

  describe 'show' do
    let(:body_2) { 'Whoop there it is' }
    let!(:argument_1) { FactoryGirl.create(:argument, topic: topic, perspective: Perspective.pro, body_html: body_1) }
    let!(:argument_2) { FactoryGirl.create(:argument, topic: topic, perspective: Perspective.con, body_html: body_2) }
    it 'renders' do
      get "#{base_url}/#{topic.slug}"
      expect(response.code).to eq('200')
      expect(response).to render_template :show
      expect(response.body).to match body_1
      expect(response.body).to match body_2
    end
  end

  describe 'edit' do
    let!(:argument) { FactoryGirl.create(:argument, topic: topic, perspective: Perspective.pro, body_html: body_1) }
    context 'existing argument' do
      it 'renders' do
        get "#{base_url}/#{topic.slug}/edit"
        expect(response.code).to eq('200')
        expect(assigns(:perspective)).to eq Perspective.pro
        expect(assigns(:argument)).to eq argument
        expect(response).to render_template :edit
      end
    end
    context 'no existing argument' do
      it 'renders' do
        get "#{base_url}/#{topic.slug}/edit?perspective=con"
        expect(response.code).to eq('200')
        expect(assigns(:perspective)).to eq Perspective.con
        expect(assigns(:argument).id).to be_blank
        expect(response).to render_template :edit
      end
    end
  end
end
