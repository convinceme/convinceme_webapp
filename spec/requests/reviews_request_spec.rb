# frozen_string_literal: true
require 'rails_helper'

RSpec.describe 'ReviewsController', type: :request do
  let(:perspective) { FactoryGirl.create(:perspective) }
  let(:repository) { perspective.repository }
  let(:commit_review) { FactoryGirl.create(:commit_review, repository: repository) }
  describe 'show' do
    it 'renders' do
      commit_review.create_output_file
      get "/reviews/#{repository}/#{commit_review.sha}"
      expect(assigns(:commit_review)).to eq(commit_review)
      expect(response.code).to eq('200')
      expect(response).to render_template :show
      expect(response.body).to be_present
    end
  end
end
