# frozen_string_literal: true
require 'rails_helper'

RSpec.describe 'VotesController', type: :request do
  let(:topic) { FactoryGirl.create(:topic) }
  let(:argument) { FactoryGirl.create(:argument, topic: topic) }
  describe 'create' do
    let(:target_redirect_url) { "/#{topic.slug}" }
    let(:vote_attrs) { { argument_id: argument.id, kind: 'quality' } }

    context 'logged in user' do
      let(:user) { FactoryGirl.create(:user) }
      before { sign_in user }
      context 'allowed to vote' do
        it 'votes' do
          expect(user.votes.count).to eq 0
          post '/votes', params: { vote: vote_attrs },
                         headers: { referer: target_redirect_url }

          expect(flash[:error]).to_not be_present
          expect(response).to redirect_to target_redirect_url
          user.reload
          vote = user.votes.first
          vote_attrs.each { |k, v| expect(vote.send(k)).to eq(v) }
        end
      end
      context 'not allowed to vote' do
        it 'does not vote, displays flash' do
          FactoryGirl.create(:vote, argument: argument, user: user, kind: 'quality')
          expect do
            post '/votes', params: { vote: vote_attrs },
                           headers: { referer: target_redirect_url }

            expect(flash[:error]).to be_present
            expect(response).to redirect_to target_redirect_url
          end.to be_changed(Vote, :count).by 0
        end
      end
      context 'invalid topic' do
        it 'does not vote, display flash' do
          post '/votes', params: { vote: vote_attrs.merge(argument_id: 3939393) },
                         headers: { referer: target_redirect_url }

          expect(response).to redirect_to target_redirect_url
          expect(flash[:error]).to be_present
        end
      end
    end
    context 'no user signed in' do
      let(:target_vote_session) { vote_attrs.merge(argument_id: vote_attrs[:argument_id].to_s) }
      it 'redirects to sign in path and sets the session' do
        post '/votes', params: { vote: vote_attrs },
                       headers: { referer: target_redirect_url }

        expect(response).to redirect_to new_user_session_path
        expect(session[:after_login_vote]).to eq target_vote_session
      end
    end
  end
end
