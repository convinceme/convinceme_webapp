# frozen_string_literal: true
require 'rails_helper'

RSpec.describe ApplicationHelper, type: :helper do
  describe 'argument_links' do
    let(:perspective_hashes) do
      [
        {
          perspective_id: 11,
          argument_id: 1,
          enum_letter: 'A'
        },
        {
          perspective_id: 12,
          argument_id: 2,
          enum_letter: 'B'
        },
        {
          perspective_id: 13,
          argument_id: 3,
          enum_letter: 'C'
        }
      ]
    end

    let(:target) do
      '<form class="button_to" method="post" action="/votes">' \
        '<input type="submit" value="A" />' \
        '<input type="hidden" name="vote[argument_id]" value="1" />' \
        '<input type="hidden" name="vote[kind]" value="quality" />' \
        '</form>, '\
        '<form class="button_to" method="post" action="/votes">' \
        '<input type="submit" value="B" />' \
        '<input type="hidden" name="vote[argument_id]" value="2" />' \
        '<input type="hidden" name="vote[kind]" value="quality" />' \
        '</form> or ' \
        '<form class="button_to" method="post" action="/votes">' \
        '<input type="submit" value="C" />' \
        '<input type="hidden" name="vote[argument_id]" value="3" />' \
        '<input type="hidden" name="vote[kind]" value="quality" />' \
        '</form>?'
    end
    it 'returns a list of links as a sentence' do
      expect(argument_links(perspective_hashes, kind: 'quality')).to eq target
    end
  end
end
