## Warning - WIP

This project is undergoing a major overhaul, most of everything is being revised.

Things aren't functional. Expect ongoing major revisions and for things to not work correctly. You can view a functioning version of the project on [github](https://github.com/convinceme/webapp) - which is hosted at [convinceme.us](https://www.convinceme.us/).

-------------------

ConvinceMe; check out [the info repository](https://gitlab.com/convinceme/convinceme_content/) for more info

This is the app that displays everything.

It uses ruby, postgres and redis.

You can start it with `./start`

Run tests with `bundle exec guard`

-------------------

- [License](LICENSE)
- [Code of Conduct](https://gitlab.com/convinceme/convinceme_content/blob/master/code_of_conduct.markdown)
